package com.example.chua.midterm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    String dataFromScan;
    TextView mtotal;
    ListView listV;
    ArrayAdapter<String> shop;
    ArrayList<String> list = new ArrayList<String>();
    double total = 0.0;
    int quantity;
    double price;
    String message = "";
    Button Send ;
    ImageButton Cam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mtotal = (TextView) findViewById(R.id.mtxtTotal);
        listV = (ListView) findViewById(R.id.listV);
        Cam = (ImageButton)findViewById(R.id.mbtnCam);
        shop = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        listV.setAdapter(shop);


        Send = (Button) findViewById(R.id.mbtnSend);
        Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(total != 0) {
                    message += "Total: " + total;
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage("09062274665", null, message, null, null);
                    Toast.makeText(MainActivity.this, "Shopping list sent :)!", Toast.LENGTH_LONG).show();
                }else
                    Toast.makeText(MainActivity.this, "Pls scan first before send :)!", Toast.LENGTH_LONG).show();
            }
        });

        Cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(MainActivity.this, ScannerView.class);
                startActivityForResult(n, 1);
            }
        });
    }


    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, Scanner.class);
        startActivityForResult(intent, 1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (data != null) {
                dataFromScan = data.getStringExtra("shopp");
                    try {


                    String[] split = dataFromScan.split("\\|\\|\\|");
                    price = Double.parseDouble(split[2]);
                    quantity = Integer.parseInt(split[1]);
                    total += price * quantity;
                    String item = split[0].toString();
                    String temp = item + ", " + quantity + ", " + price;

                    list.add(temp);
                    shop.notifyDataSetChanged();

                    message += temp + "\n";

                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "Invalid qr detected please scan another QR code", Toast.LENGTH_LONG).show();
                    }
                }

            mtotal.setText(total + " ");
            }
        }
    }