package com.example.chua.midterm;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.xml.transform.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Chua on 9/6/2016.
 */
public class ScannerView extends AppCompatActivity implements ZXingScannerView.ResultHandler{

    private ZXingScannerView mScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(com.google.zxing.Result result) {
        String message = result.getText();
        Intent intent = new Intent();
        intent.putExtra("shopp",message);
        setResult(2,intent);
        finish();
    }
}